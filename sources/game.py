import pygame
import time
import random
pygame.init()


## POLICES ##
police = pygame.font.SysFont("monospace" ,30)
police2 = pygame.font.SysFont("SuperBubble-Rpaj3" ,45)
police3 = pygame.font.SysFont("SuperBubble-Rpaj3" ,30)
police4 = pygame.font.SysFont("SuperBubble-Rpaj3" ,55)
police5 = pygame.font.SysFont("SuperBubble-Rpaj3" ,70)
police6 = pygame.font.SysFont("SuperBubble-Rpaj3" ,40)

image_texte = police.render ( "score :", 1 , (255,255,255))
menu_texte = police2.render("MENU",1,(255,255,255))


# SON
pygame.mixer.init()
sound_frappe = pygame.mixer.Sound('breath.wav')
sound_frappesac = pygame.mixer.Sound('punch.mp3')
sound_frappecombot = pygame.mixer.Sound('powerpunch.mp3')
sound_fight = pygame.mixer.Sound('fight.mp3')
#sound_clap = pygame.mixer.Sound('clap.mp3')
sound_setting = True


#score/niveau
score = 1000
multiplicateur = 1
image_score = pygame.image.load("score.png")
niveau_joueur = 1
run = False
nom_du_joueur = input('Quel est votre pseudo ? ')
pseudo_joueur = police2.render(nom_du_joueur,1,(255,255,255))
clicker = police2.render(str(multiplicateur) + "  / clic",1,(255,255,255))

#generer la screen etre de notre jeu
pygame.display.set_caption("Boxe")
screen = pygame.display.set_mode((1380,776))
background = pygame.image.load('fond.jpg')
screen.blit(background, (0,0))

## MENU ##
# texte du menu #
entrainement_texte = police4.render("ENTRAINEMENT",1,(255,255,255))
niveaux_texte = police4.render("NIVEAUX",1,(255,255,255))
menu_title = police5.render("BOXING CLICKER GAME",1,(255,255,255))
menu_title2 = police5.render("BOXING CLICKER GAME",1,(4,56,177))
# image du menu #
screen_menu = pygame.image.load("menu_screen.jpg")
menu_case = pygame.image.load("menu_case.jpg")
# animation des boutons du menu #
menu_case_clicked = pygame.image.load("menu_case_clicked.jpg")
entrainement_texte_clicked = police4.render("ENTRAINEMENT",1,(121,146,242))
niveaux_texte_clicked = police4.render("NIVEAUX",1,(121,146,242))

## SETTINGS ##
# texte du menu settings #
reglage_text = police4.render("SETTINGS",1,(255,255,255))
pseudo_text = police6.render("PSEUDO :",1,(255,255,255))
sound_text = police6.render("SOUND   :",1,(255,255,255))
active_text = police3.render("ACTIF",1,(255,255,255))
desactive_text = police3.render("DESACTIVE",1,(255,255,255))



## SHOP ##
# liste de booléen pour savoir si oui ou non un objet a était acheter #
buyed1 = [False,False,False]
buyed2 = [False,False,False]
buyed3 = [False,False,False]
# images des items du shop #
gant_bleu_shop = pygame.image.load("gant_bleu_shop.png")
gant_vert_shop = pygame.image.load("gant_vert_shop.png")
gant_orange_shop = pygame.image.load("gant_orange_shop.png")
gant_violet_shop = pygame.image.load("gant_violet_shop.png")
sac_bleu_shop = pygame.image.load("sac_bleu_shop.png")
sac_vert_shop = pygame.image.load("sac_vert_shop.png")
sac_orange_shop = pygame.image.load("sac_orange_shop.png")
sac_violet_shop = pygame.image.load("sac_violet_shop.png")
gant_noir_shop = pygame.image.load("gant_noir_shop.png")
# images du shop #
gray_case = pygame.image.load("case_grise.png")
score_gray = pygame.image.load("score_gray.png")
shop_gray = pygame.image.load("shop_buy_gray.png")
shop_case_gray = pygame.image.load("shop_case_gray.png")
shop_buy_gray = pygame.image.load("shop_buy_gray.png")
shop_case = pygame.image.load("shop_case.png")
shop_buy = pygame.image.load("shop_buy.png")
shop_score = pygame.image.load("score.png")
buy_clicked = pygame.image.load("buy_clicked.png")
# textes du shop #
buy_texte_clicked = police2.render("BUY",1,(121,146,242))
buy_texte = police2.render("BUY",1,(255,255,255))
buy_texte_red = police2.render("BUY",1,(209,16,26))
text_buy_red = police2.render("BUY",1,(255,255,255))
buyed_texte_gray = police2.render("BUYED",1,(68,68,68))
shop_texte = police2.render("SHOP",1,(255,255,255))
shop_texte_clicked = police2.render("SHOP",1,(12,33,120))
suite_text = police6.render("NEXT",1,(255,255,255))
retour_text = police6.render("BACK",1,(255,255,255))

# image
sac = pygame.image.load("sac.png").convert_alpha()
pos2 = pygame.image.load("perso_1.png").convert_alpha()
pos0 = pygame.image.load("perso_0.png").convert_alpha()
shop = pygame.image.load("shop.png")
barre_shop = pygame.image.load("barre_shop.png")
barre_menu = pygame.image.load("barre_menu.png")
barre_shop_clicked = pygame.image.load("barre_shop_clicked.png")
perso_menu = pygame.image.load("perso_menu.png")
dent = pygame.image.load("dent.png")
dent_clicked = pygame.image.load("dent_clicked.png")
barre_menu_clicked = pygame.image.load("barre_menu_clicked.png")
reglage = pygame.image.load("reglage.jpg")
reglage_button = pygame.image.load("reglage_button2.jpg")
reglage_button_2 = pygame.image.load("reglage_button3.jpg")
pseudo_setting = True

def co_score(score):
    """
    retourne le score du joueur a la bonne position
    param score : score du joueur
    return tuple : coordonnée du score dans le jeu
    """
    if score < 100 :
        return image_texte, (190,9)
    elif 99 < score < 1000 :
        return image_texte, (170,9)
    elif 999 < score < 10000:
        return image_texte, (150,9)
    elif 9999 < score < 100000 :
        return image_texte2, (170,9)
    elif 99999 < score < 1000000:
        return image_texte2, (150,9)
    elif 999999 < score < 10000000:
         return image_texte3, (190,9)
    elif 9999999 < score < 100000000:
         return image_texte3, (170,9)
    else:
        return image_texte3, (150,9)




def enough_score(y,z):
    """
    fait apparaitre le shop et montré en gris les objets deja acheter ou pour lesquelles le joueur n'a pas assez de score
    param : y => score, z => tuple des prix
    
    """
    enough = [False,False,False]
    for i in range(3):
        # assez pour aucun
        if y < z[0]:
            enough = [False,False,False]
        # asser pour le 1er
        elif y < z[1]:
            enough = [True,False,False]
        #assez pour le 1er et 2eme
        elif y < z[2]:
            enough = [True,True,False]
        #assez pour les 3
        else:
            enough = [True,True,True]
    return enough

def shop_price(price,color,x,i):
    """
    retourne le prix d'un item dans le shop
    param : price => prix de l'item,
    return : str
    """ 
    if color == 'gray':
        colors = (127,127,127)
        if price[i] <= 999 :
            price_texte_gray = police3.render(str(price[i]),1,colors)
        elif price[i] <= 9999:
            price_texte_gray = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        else:
            price_texte_gray = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        screen.blit(shop_case_gray, (50,x))
        screen.blit(shop_buy_gray, (250,x + 35))
        screen.blit(price_texte_gray,(105,x + 113))
        screen.blit(shop_score,(145,x + 110))
        screen.blit(buyed_texte_gray,(280,x + 60))
        
    elif color == 'red' :
        colors = (209,16,26)
        if price[i] <= 999 :
            price_texte_red = police3.render(str(price[i]),1,colors)
        elif price[i] <= 9999:
            price_texte_red = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        else:
            price_texte_red = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        screen.blit(shop_case, (50,x))
        screen.blit(shop_buy, (250,x + 35))
        screen.blit(price_texte_red,(105,x + 113))
        screen.blit(shop_score,(145,x + 110))
        screen.blit(buy_texte_red,(297,x + 60))
    else:
        colors = (255,255,255)
        if price[i] <= 999 :
            price_texte = police3.render(str(price[i]),1,colors)
        elif price[i] <= 9999:
            price_texte = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        else:
            price_texte = police3.render(str(int(round(price[i] / 1000, 0))) + "k",1,colors)
        screen.blit(shop_case, (50,x))
        screen.blit(shop_buy, (250,x + 35))
        screen.blit(price_texte,(105,x + 113))
        screen.blit(shop_score,(145,x + 110))
        screen.blit(buy_texte,(297,x + 60))
    

    

def all_shop2(acheter,score,price):
    enough = enough_score(score,price)
    screen.blit(shop, (0,45))
    #PREMIERE CASE
    if acheter[0]:
        screen.blit(gray_case,(0,45))
        shop_price(price,"gray",90,0)
    elif enough[0] == False:
        shop_price(price,"red",90,0)
    else:
        shop_price(price,"",90,0)
    #DEUXIEME CASE
    if acheter[1]:
        screen.blit(gray_case,(0,265))
        shop_price(price,"gray",310,1)
    elif enough[1] == False:
        shop_price(price,"red",310,1)
    else:
        shop_price(price,"",310,1)
    #TROISIEME CASE
    if acheter[2]:
        screen.blit(gray_case,(0,485))
        shop_price(price,"gray",530,2)
    elif enough[2] == False:
        shop_price(price,"red",530,2)
    else:
        shop_price(price,"",530,2)
    screen.blit(suite_text,(380,675))
    



def collision_sac(x):
    if x + 30 > 460:
        return True
    return False

def score_per_clic(x):
    """
    renvoie le nombre de score par clic
    param : x => multiplicateur (score par clic)
    return : str
    """
    if multiplicateur < 9 :
        screen.blit(clicker,(1260,55))
        screen.blit(image_score, (1280,55))
    elif multiplicateur < 99:
        screen.blit(clicker,(1250,55))
        screen.blit(image_score, (1285,55))
    elif multiplicateur < 999:
        screen.blit(clicker,(1235,55))
        screen.blit(image_score, (1285,55))
    else:
        screen.blit(clicker,(1215,55))
        screen.blit(image_score, (1280,55))
#texte
image_texte = police.render (str(int(round(score,0))), 1 , (255,255,255))
image_texte2 = police.render (str(int(round(score / 1000,10))) + "k", 1 , (255,255,255))
niveau_texte = police2.render("NIVEAU " + str(niveau_joueur),1,(255,255,255))

x = 0
y = 0
clock = pygame.time.Clock()

frappes = 0

running = True
while running:
    screen.blit(screen_menu, (0,0))
    screen.blit(menu_case, (508,200))
    screen.blit(menu_case, (508,400))
    screen.blit(menu_case, (508,600))
    screen.blit(entrainement_texte,(534,240))
#     screen.blit(niveaux_texte,(606,438))
    screen.blit(menu_title2, (404,54))
    screen.blit(menu_title, (400,50))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
            print("Fermeture du jeu")
        elif event.type == pygame.MOUSEBUTTONUP:
            pos_souris = pygame.mouse.get_pos()
            if 513 < pos_souris[0] < 868 and 208 < pos_souris[1] < 306 :
                ##animation##
                screen.blit(screen_menu, (0,0))
                screen.blit(menu_title2, (404,54))
                screen.blit(menu_title, (400,50))
                screen.blit(menu_case, (508,400))
                screen.blit(menu_case, (508,600))
#                 screen.blit(niveaux_texte,(606,438))
                screen.blit(menu_case_clicked, (508,200))
                screen.blit(entrainement_texte_clicked, (534,240))
                pygame.display.flip()
                time.sleep(0.1)
                #############0
                # entré dans la partie "entrainement"
                sound_fight.play()
                run2 = True
                while run2:
                    #arriere plan
                    screen.blit(background, (0,0))
                    
                    # texte
                    image_texte = police.render (str(score), 1 , (255,255,255))
                    image_texte2 = police.render (str(int(round(score / 1000,10))) + "k", 1 , (255,255,255))
                    image_texte3 = police.render (str(int(round(score / 1000000,10))) + "M", 1 , (255,255,255))
                    niveau_texte = police2.render("NIVEAU " + str(niveau_joueur),1,(255,255,255))
                    clicker = police6.render(str(multiplicateur) + "    / clic",1,(255,255,255))
                    
                    # images
                    screen.blit(sac, (600, -70))
                    screen.blit(barre_shop, (0,0))
                    screen.blit(barre_menu, (1220,0))
                    screen.blit(menu_texte, (1277,11))
                    screen.blit(dent,(1230,11))
                    score_per_clic(multiplicateur)
                    screen.blit(co_score(score)[0],co_score(score)[1])
                    screen.blit(shop_texte, (14,11))
                    screen.blit(image_score, (230,12))
                    screen.blit(niveau_texte, (292,11))
                    # deplacement/frappe
                    pressed = pygame.key.get_pressed()
                    if (pressed[pygame.K_d] or pressed[pygame.K_RIGHT]) and x < 450:
                        x += 4
                    if (pressed[pygame.K_q] or pressed[pygame.K_LEFT]) and x > 0:
                        x -= 4
                    if pressed[pygame.K_z] or pressed[pygame.K_UP]:
                        screen.blit(pos2, (x, 150))
                        screen.blit(pseudo_joueur, (x + 140,120))
#                         pygame.display.flip()
#                         time.sleep(0.1)
#                         screen.blit(background, (0,0))
#                         screen.blit(sac, (600, -70))
#                         screen.blit(barre_shop, (0,0))
#                         score_per_clic(multiplicateur)
#                         player_score(score)
#                         screen.blit(shop_texte, (14,11))
#                         screen.blit(barre_menu, (1220,0))
#                         screen.blit(menu_texte, (1277,11))
#                         screen.blit(dent,(1230,11))
#                         screen.blit(image_score, (230,12))
#                         screen.blit(niveau_texte, (292,11))
#                         screen.blit(pos0, (x, 150))
#                         screen.blit(pseudo_joueur, (x + 140,120))
                        pygame.display.flip()
                        if sound_setting:
                            sound_frappe.play()
                        release = True
                        while release :
                            for event in pygame.event.get():
                                if event.type == pygame.KEYUP and (event.key == pygame.K_UP or event.key == pygame.K_z):
                                    release = False
                    else:
                        screen.blit(pos0, (x, 150))
                        screen.blit(pseudo_joueur, (x + 140,120))
                    ######
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN and (event.key == pygame.K_UP or event.key == pygame.K_z) and collision_sac(x) and sound_setting:
                            score += multiplicateur
                            sound_frappesac.play()
                            frappes += 1
                            if frappes == 10:
                                #sound_clap.play()
                                sound_frappecombot.play()
                                frappes = 0
                        elif event.type == pygame.KEYDOWN and (event.key == pygame.K_UP or event.key == pygame.K_z) and collision_sac(x):
                            score += multiplicateur
                        #quitter le jeu
                        elif event.type == pygame.QUIT:
                            run2 = False
                            pygame.quit()
                            print("Fermeture du jeu")
                        #shop
                        elif event.type == pygame.MOUSEBUTTONUP:
                            pos_souris = pygame.mouse.get_pos()
                            if 0 < pos_souris[0] < 112 and 0 < pos_souris[1] < 42:
                                pos_souris = (0,0)
                                screen.blit(barre_shop_clicked, (0,0))
                                screen.blit(shop_texte_clicked, (14,11))
                                pygame.display.flip()
                                time.sleep(0.1)
                                screen.blit(barre_shop, (0,0))
                                screen.blit(shop_texte, (14,11))
                                screen.blit(co_score(score)[0],co_score(score)[1])
                                screen.blit(shop_texte, (14,11))
                                screen.blit(image_score, (230,12))
                                screen.blit(niveau_texte, (292,11))
                                run = True
                                while run:
                                    ##PREMIERE PAGE DU SHOP
                                    all_shop2(buyed1,score,(100,500,1000))
                                    screen.blit(gant_bleu_shop,(90,98))
                                    screen.blit(gant_vert_shop,(90,538))
                                    screen.blit(sac_bleu_shop,(101,315))
                                    pygame.display.flip()
                                    for event in pygame.event.get():
                                        #quitter le jeu
                                        if event.type == pygame.QUIT:
                                            run = False
                                            run2 = False
                                            pygame.quit()
                                            print("Fermeture du jeu")
                                        ############
                                        elif event.type == pygame.MOUSEBUTTONUP:
                                            pos_souris = pygame.mouse.get_pos()
                                            #quiter le shop
                                            if 0 < pos_souris[0] < 112 and 0 < pos_souris[1] < 42:
                                                screen.blit(barre_shop_clicked, (0,0))
                                                screen.blit(shop_texte_clicked, (14,11))
                                                pygame.display.flip()
                                                time.sleep(0.1)
                                                run = False
                                            elif not(5 < pos_souris[0] < 478 and 45 < pos_souris[1] < 710):
                                                run = False
                                            #premier buy
                                            elif  254 < pos_souris[0] < 406 and 140 < pos_souris[1] < 202 and score >=  100 and buyed1[0] == False:
                                                score -= 100
                                                multiplicateur += 1
                                                niveau_joueur += 1
                                                pos2 = pygame.image.load("perso_1_bleu.png").convert_alpha()
                                                pos0 = pygame.image.load("perso_0_bleu.png").convert_alpha()
                                                buyed1[0] = True
                                                ##animation##
                                                screen.blit(buy_clicked,(250, 125))
                                                screen.blit(buy_texte_clicked,(297,150))
                                                pygame.display.flip()
                                                time.sleep(0.1)
                                                ##############
                                                run = False
                                            #deuxieme buy
                                            elif 254 < pos_souris[0] < 406 and 340 < pos_souris[1] < 402 and score >= 500 and buyed1[1] == False:
                                                score -= 500
                                                multiplicateur += 2
                                                niveau_joueur += 3
                                                buyed1[1] = True
                                                sac = pygame.image.load("sac_bleu.png").convert_alpha()
                                                ##animation##
                                                screen.blit(buy_clicked,(250, 345))
                                                screen.blit(buy_texte_clicked,(297,370))
                                                pygame.display.flip()
                                                time.sleep(0.1)
                                                run = False
                                            #troisieme buy
                                            elif 254 < pos_souris[0] < 406 and 570 < pos_souris[1] < 632 and score >= 1000 and buyed1[2] == False:
                                                score -= 1000
                                                multiplicateur += 5
                                                niveau_joueur += 5
                                                pos2 = pygame.image.load("perso_1_vert.png").convert_alpha()
                                                pos0 = pygame.image.load("perso_0_vert.png").convert_alpha()
                                                buyed1[2] = True
                                                ##animation##
                                                screen.blit(buy_clicked,(250, 565))
                                                screen.blit(buy_texte_clicked,(297,590))
                                                pygame.display.flip()
                                                time.sleep(0.1)
                                                run = False
                                            ##DEUXIEME PAGE DU SHOP
                                            elif 360 < pos_souris[0] < 452 and 668 < pos_souris[1] < 696:
                                                run_page2 = True
                                                while run_page2:
                                                    all_shop2(buyed2,score,(2000,5000,10000))
                                                    screen.blit(sac_vert_shop,(101,95))
                                                    screen.blit(gant_orange_shop,(90,318))
                                                    screen.blit(sac_orange_shop,(101,535))
                                                    screen.blit(retour_text,(15,675))
                                                    pygame.display.flip()
                                                    for event in pygame.event.get():
                                                        #quitter le jeu
                                                        if event.type == pygame.QUIT:
                                                            run = False
                                                            run2 = False
                                                            pygame.quit()
                                                            print("Fermeture du jeu")
                                                        elif event.type == pygame.MOUSEBUTTONUP:
                                                            pos_souris = pygame.mouse.get_pos()
                                                            #quitter le shop
                                                            if 0 < pos_souris[0] < 112 and 0 < pos_souris[1] < 42:
                                                                screen.blit(barre_shop_clicked, (0,0))
                                                                screen.blit(shop_texte_clicked, (14,11))
                                                                pygame.display.flip()
                                                                time.sleep(0.1)
                                                                run = False
                                                                run_page2 = False
                                                            elif not(5 < pos_souris[0] < 478 and 45 < pos_souris[1] < 710):
                                                                run = False
                                                                run_page2 = False
                                                            #retour a la page precedente
                                                            elif 13 < pos_souris[0] < 100 and 677 < pos_souris[1] < 698:
                                                                run_page2 = False
                                                            #premier buy
                                                            elif  254 < pos_souris[0] < 406 and 140 < pos_souris[1] < 202 and score >= 2000 and buyed2[0] == False:
                                                                score -= 2000
                                                                multiplicateur += 10
                                                                niveau_joueur += 1
                                                                buyed2[0] = True
                                                                sac = pygame.image.load("sac_vert.png").convert_alpha()
                                                                ##animation##
                                                                screen.blit(buy_clicked,(250, 125))
                                                                screen.blit(buy_texte_clicked,(297,150))
                                                                pygame.display.flip()
                                                                time.sleep(0.1)
                                                                ##############
                                                                run = False
                                                                run_page2 = False
                                                            #deuxieme buy
                                                            elif 254 < pos_souris[0] < 406 and 340 < pos_souris[1] < 402 and score >= 5000 and buyed2[1] == False:
                                                                score -= 5000
                                                                multiplicateur += 20
                                                                niveau_joueur += 3
                                                                buyed2[1] = True
                                                                pos2 = pygame.image.load("perso_1_orange.png").convert_alpha()
                                                                pos0 = pygame.image.load("perso_0_orange.png").convert_alpha()
                                                                ##animation##
                                                                screen.blit(buy_clicked,(250, 345))
                                                                screen.blit(buy_texte_clicked,(297,370))
                                                                pygame.display.flip()
                                                                time.sleep(0.1)
                                                                run = False
                                                                run_page2 = False
                                                            #troisieme buy
                                                            elif 254 < pos_souris[0] < 406 and 570 < pos_souris[1] < 632 and score >= 10000 and buyed2[2] == False:
                                                                score -= 10000
                                                                multiplicateur += 50
                                                                niveau_joueur += 5
                                                                buyed2[2] = True
                                                                sac = pygame.image.load("sac_orange.png").convert_alpha()
                                                                ##animation##
                                                                screen.blit(buy_clicked,(250, 565))
                                                                screen.blit(buy_texte_clicked,(297,590))
                                                                pygame.display.flip()
                                                                time.sleep(0.1)
                                                                run = False
                                                                run_page2 = False
                                                            #TROISIEME PAGE
                                                            elif 360 < pos_souris[0] < 452 and 668 < pos_souris[1] < 696:
                                                                run_page3 = True
                                                                while run_page3:
                                                                    all_shop2(buyed3,score,(25000,50000,75000))
                                                                    screen.blit(sac_violet_shop,(101,315))
                                                                    screen.blit(gant_violet_shop,(90,98))
                                                                    screen.blit(gant_noir_shop,(90,538))
                                                                    screen.blit(retour_text,(15,675))
                                                                    pygame.display.flip()
                                                                    for event in pygame.event.get():
                                                                        #quitter le jeu
                                                                        if event.type == pygame.QUIT:
                                                                            run = False
                                                                            run2 = False
                                                                            pygame.quit()
                                                                            print("Fermeture du jeu")
                                                                        elif event.type == pygame.MOUSEBUTTONUP:
                                                                            pos_souris = pygame.mouse.get_pos()
                                                                            #quitter le shop
                                                                            if 0 < pos_souris[0] < 112 and 0 < pos_souris[1] < 42:
                                                                                screen.blit(barre_shop_clicked, (0,0))
                                                                                screen.blit(shop_texte_clicked, (14,11))
                                                                                pygame.display.flip()
                                                                                time.sleep(0.1)
                                                                                run = False
                                                                                run_page2 = False
                                                                                run_page3= False
                                                                            elif not(5 < pos_souris[0] < 478 and 45 < pos_souris[1] < 710):
                                                                                run = False
                                                                                run_page2 = False
                                                                                run_page3 = False
                                                                            #retour a la page precedente
                                                                            elif 13 < pos_souris[0] < 100 and 677 < pos_souris[1] < 698:
                                                                                run_page3 = False
                                                                            #premier buy
                                                                            elif  254 < pos_souris[0] < 406 and 140 < pos_souris[1] < 202 and score >= 25000 and buyed3[0] == False:
                                                                                score -= 25000
                                                                                multiplicateur += 250
                                                                                niveau_joueur += 1
                                                                                buyed3[0] = True
                                                                                pos2 = pygame.image.load("perso_1_violet.png").convert_alpha()
                                                                                pos0 = pygame.image.load("perso_0_violet.png").convert_alpha()
                                                                                ##animation##
                                                                                screen.blit(buy_clicked,(250, 125))
                                                                                screen.blit(buy_texte_clicked,(297,150))
                                                                                pygame.display.flip()
                                                                                time.sleep(0.1)
                                                                                ##############
                                                                                run = False
                                                                                run_page2 = False
                                                                                run_page3 = False
                                                                            #deuxieme buy
                                                                            elif 254 < pos_souris[0] < 406 and 340 < pos_souris[1] < 402 and score >= 50000 and buyed3[1] == False:
                                                                                score -= 50000
                                                                                multiplicateur += 500
                                                                                niveau_joueur += 3
                                                                                buyed3[1] = True
                                                                                sac = pygame.image.load("sac_violet.png").convert_alpha()
                                                                                ##animation##
                                                                                screen.blit(buy_clicked,(250, 345))
                                                                                screen.blit(buy_texte_clicked,(297,370))
                                                                                pygame.display.flip()
                                                                                time.sleep(0.1)
                                                                                run = False
                                                                                run_page2 = False
                                                                                run_page3 = False
                                                                            #troisieme buy
                                                                            elif 254 < pos_souris[0] < 406 and 570 < pos_souris[1] < 632 and score >= 75000 and buyed3[2] == False:
                                                                                score -= 75000
                                                                                multiplicateur += 1000
                                                                                niveau_joueur += 5
                                                                                buyed3[2] = True
                                                                                pos2 = pygame.image.load("perso_1_noir.png").convert_alpha()
                                                                                pos0 = pygame.image.load("perso_0_noir.png").convert_alpha()
                                                                                ##animation##
                                                                                screen.blit(buy_clicked,(250, 565))
                                                                                screen.blit(buy_texte_clicked,(297,590))
                                                                                pygame.display.flip()
                                                                                time.sleep(0.1)
                                                                                run = False
                                                                                run_page2 = False
                                                                                run_page3 = False    
                                                    
                                            pygame.display.flip()# <= pour voir l'animation du buy
                                pygame.display.flip()
                            # retour au menu
                            elif 1266 < pos_souris[0] < 1375 and 4 < pos_souris[1] < 42 :
                                run2 = False
                            #ouvrir le menu reglage
                            elif 1224 < pos_souris[0] < 1262 and 4 < pos_souris[1] < 42 :
                                screen.blit(barre_menu_clicked,(1220,0))
                                screen.blit(dent_clicked,(1230,11))
                                pygame.display.flip()
                                time.sleep(0.1)
                                screen.blit(barre_menu,(1220,0))
                                screen.blit(menu_texte, (1277,11))
                                screen.blit(dent,(1230,11))
                                run_reglage = True
                                while run_reglage:
                                    screen.blit(reglage, (500,150))
                                    screen.blit(reglage_text, (575,170))
                                    screen.blit(pseudo_text, (530,260))
                                    screen.blit(sound_text, (530,340))
                                    if pseudo_setting:
                                        screen.blit(reglage_button,(757,250))
                                        screen.blit(active_text, (775,265))
                                    else:
                                        screen.blit(reglage_button_2,(720,250))
                                        screen.blit(desactive_text, (728,265))
                                    if sound_setting:
                                        screen.blit(reglage_button,(757,330))
                                        screen.blit(active_text, (775,345))
                                    else:
                                        screen.blit(reglage_button_2,(720,330))
                                        screen.blit(desactive_text, (728,345))
                                    pygame.display.flip()
                                    
                                    for event in pygame.event.get():
                                        if event.type == pygame.QUIT:
                                            run2 = False
                                            running = False
                                            run_reglage = False
                                            pygame.quit()
                                            print("Fermeture du jeu")
                                        elif event.type == pygame.MOUSEBUTTONUP:
                                            pos_souris = pygame.mouse.get_pos()
                                            if 1224 < pos_souris[0] < 1251 and 3 < pos_souris[1] < 43:
                                                screen.blit(barre_menu_clicked,(1220,0))
                                                screen.blit(dent_clicked,(1230,11))
                                                pygame.display.flip()
                                                time.sleep(0.1)
                                                run_reglage = False
                                            elif not(500 < pos_souris[0] < 860 and 150 < pos_souris[1] < 615):
                                                run_reglage = False
                                            elif 761 < pos_souris[0] < 845 and 256 < pos_souris[1] < 291 and pseudo_setting:
                                                pseudo_setting = False
                                                pseudo_joueur = police2.render("",1,(255,255,255))
                                            elif 725 < pos_souris[0] < 845 and 256 < pos_souris[1] < 291 and pseudo_setting == False:
                                                pseudo_setting = True
                                                pseudo_joueur = police2.render(nom_du_joueur,1,(255,255,255))
                                            elif 761 < pos_souris[0] < 845 and 336 < pos_souris[1] < 370 and sound_setting:
                                                sound_setting = False
                                            elif 725 < pos_souris[0] < 845 and 336 < pos_souris[1] < 370 and sound_setting == False:
                                                sound_setting = True

                            ##voir les stats du perso
                            elif 262 < pos_souris[0] < 454 and 5 < pos_souris[1] < 42:
                                run3 = True
                                while run3:
                                    #arriere plan
                                    screen.blit(perso_menu, (0,45))
                                    screen.blit(gant_bleu_shop,(50,600))
                                    pygame.display.flip()
                                    for event in pygame.event.get():
                                        #quitter le jeu
                                        if event.type == pygame.QUIT:
                                            run3 = False
                                            run2 = False
                                            pygame.quit()
                                            print("Fermeture du jeu")
                                        ############
                                        elif event.type == pygame.MOUSEBUTTONUP:
                                            pos_souris = pygame.mouse.get_pos()
                                            print(pos_souris)
                                            #quiter le menu
                                            if 262 < pos_souris[0] < 454 and 5 < pos_souris[1] < 42:
                                                run3 = False
                    pygame.display.flip()
#             #bouton niveaux
#             elif 513 < pos_souris[0] < 869 and 405 < pos_souris[1] < 503 :
#                 ##animation##
#                 screen.blit(screen_menu, (0,0))
#                 screen.blit(menu_case, (508,200))
#                 screen.blit(menu_case, (508,600))
#                 screen.blit(entrainement_texte,(534,240))
#                 screen.blit(menu_title2, (404,54))
#                 screen.blit(menu_title, (400,50))
#                 screen.blit(menu_case_clicked, (510,402))
#                 screen.blit(niveaux_texte_clicked, (608,440))
#                 pygame.display.flip()
#                 time.sleep(0.1)
                #############
                # entré dans la partie "niveaux"
                    
    
    
                                
        
        
    

    
    pygame.display.flip()
    #mettre a jour l'écran
    clock.tick(300)

pygame.quit()
        
            
